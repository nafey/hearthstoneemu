﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Stone.Game.Rules;

namespace Stone.Game {
    class Game {
        private List<Player> lstPlayer;
        private Random rnd;
        private Player active;

        public Game() {
            rnd = new Random();
            lstPlayer = new List<Player>();
            lstPlayer.Add(new Player(0, TriggerRules));
            lstPlayer.Add(new Player(1, TriggerRules));

            Initialize();
        }

        //Setup the players at start of the game
        public void Initialize() {
            //decide which player gets to go first
            int first = GetRandomNumber(this.lstPlayer.Count);

            //set it as the active player
            this.active = lstPlayer[first];

            //make active go first, rest go later
            foreach (Player pl in lstPlayer) {
                if (pl == active) {
                    pl.GoFirst();
                }
                else {
                    pl.GoSecond();
                }
            }

            //Begin the turn for the other player
            this.active.BeginTurn();
        }
        
        //Make stuff happen :)
        public void DoEffect(Effect effect, Card card, Player player) {
            if (effect.GetType() == EffectType.DrawCard) {
                DrawCardEffect drawCardEffect = (DrawCardEffect)effect;
                this.DrawCard(player);
            }
        }

        public void TriggerRules(RuleTrigger trigger) {
            //BattleCry
            if (trigger == RuleTrigger.BattleCry) {
                foreach (Rule r in this.active.GetPlayedCard().GetRules()) {
                    this.DoEffect(r.GetEffect(), this.active.GetPlayedCard(), this.active);
                }
                return;
            }

            //DeathRattle
            if (trigger == RuleTrigger.DeathRattle) {
                foreach (Player p in this.lstPlayer) {
                    foreach (Card c in p.GetLimbo()) {
                        foreach (Rule r in c.GetRules()) {
                            this.DoEffect(r.GetEffect(), c, p);
                        }
                    }
                }
                return;
            }

            //Everything else
            foreach (Player p in lstPlayer) {
                foreach (Card c in p.GetTable()) {
                    foreach (Rule r in c.GetRules()) {
                        if (r.GetTrigger() == trigger) {
                            this.DoEffect(r.GetEffect(), c, p); 
                        }
                    }
                }
            }
        }

        //Returns a random number between 0 and max
        public int GetRandomNumber(int max) {
            int r = this.rnd.Next(max);
            return r;
        }

        //get next player in the player list from the active player
        public Player GetNextPlayer() {

            for (int i = 0; i < lstPlayer.Count(); i++) {
                if (active == lstPlayer[i]) {
                    if (i == lstPlayer.Count() - 1) {
                        return lstPlayer[0];
                    }
                    else {
                        return lstPlayer[i + 1];
                    }
                }
            }

            return null;
        }

        //Build a PlayerInfo object and pass it on
        public List<PlayerInfo> GetPlayerInfo() {
            List<PlayerInfo> ret = new List<PlayerInfo>();
            foreach (Player p in lstPlayer) {
                ret.Add(p.GetPlayerInfo());
            }

            return ret;
        }

        //Draw a card for the player
        public void DrawCard(Player p) {
            p.Draw();
        }

        //The function which passes control of turn among players
        public void TurnEndAction() {
            //End the turn for active player
            this.active.EndTurn();

            //Change the active player 
            this.active = this.GetNextPlayer();

            //Begin the turn for the other player
            this.active.BeginTurn();

        }

        //Summon from Hand
        public void PlayCardAction(int i) {
            this.active.PlayFromHand(i);
        }

        //Attack the target player
        public void AttackFaceAction(int attackerIndex, int targetPlayerIndex) {
            //Select the target
            this.active.SetTarget(this.lstPlayer[targetPlayerIndex]);

            this.active.AttackFace(attackerIndex);
        }

        //Attack the target minion
        public void AttackMinionAction(int attackerIndex, int defenderIndex, int targetPlayerIndex) {
            this.active.SetTarget(this.lstPlayer[targetPlayerIndex]);

            this.active.AttackMinion(attackerIndex, defenderIndex);
        }
    }

    public enum RuleTrigger {
        BeginTurn,
        EndTurn,
        BattleCry,
        DeathRattle
    }

    public enum CardType {
        Minion,
        Spell,
        Weapon,
        Secret,
        Unknown
    }
}
