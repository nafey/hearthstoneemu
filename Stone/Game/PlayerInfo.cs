﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stone.Game {
    class PlayerInfo {
        private List<CardFacade> hand;
        private List<CardFacade> table;
        private int deckSize;
        private int health;
        private int totalMana;
        private int currentMana;
        private String name;
        private bool isMyTurn;

        public PlayerInfo(List<CardFacade> hand, List<CardFacade> table,
                          int deckSize, int health,
                          int currentMana, int totalMana,
                          String name, bool isMyTurn) {
            this.hand = hand;
            this.table = table;
            this.deckSize = deckSize;
            this.health = health;
            this.currentMana = currentMana;
            this.totalMana = totalMana;
            this.name = name;
            this.isMyTurn = isMyTurn;
        }

        public String GetName() {
            return name;
        }

        public List<CardFacade> GetHand() {
            return hand;
        }

        public List<CardFacade> GetTable() {
            return table;
        }

        public int GetDeckSize() {
            return deckSize;
        }

        public int GetHealth() {
            return health;
        }

        public int GetTotalMana() {
            return totalMana;
        }

        public int GetCurrentMana() {
            return currentMana;
        }

        public bool GetIsMyTurn() {
            return isMyTurn;
        }
    }
}
