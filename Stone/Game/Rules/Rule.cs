﻿using Stone.Game;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stone.Game.Rules {
    class Rule {
        private Condition condition;
        private Effect effect;
        private RuleTrigger trigger;

        public Rule(Condition condition, Effect effect, RuleTrigger phase) {
            this.trigger = phase;
            this.condition = condition;
            this.effect = effect;
        }

        public RuleTrigger GetTrigger() {
            return this.trigger;
        }
        
        public bool Check(Game game) {
            return this.condition.Check();
        }

        public Effect GetEffect() {
            return this.effect;
        }

    }

}
