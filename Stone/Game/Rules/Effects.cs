﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stone.Game.Rules{


    public enum EffectType {
        DrawCard,
        Unknown
    }

    public enum TargetType {
        Self,
        TargetPlayer,
        Unknown
    }

    public class Effect {
        private EffectType type;

        public Effect(EffectType type) {
            this.type = type;
        }

        public EffectType GetType() {
            return type;
        }
    }

    public class DrawCardEffect : Effect {
        private TargetType target;
        private int number;

        public DrawCardEffect(bool self, int number) : base(EffectType.DrawCard) {
            if (self) {
                this.target = TargetType.Self;
            }
            else {
                this.target = TargetType.TargetPlayer;
            }

            this.number = number;
        }

        public TargetType GetTarget() {
            return target;
        }

        public int GetNumber() {
            return number;
        }
    }
}
