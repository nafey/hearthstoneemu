﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stone.Game {
    class DeckFactory {
        public DeckFactory() {
        }
        public List<Card> getDeck(int idx) {
            List<Card> deck = new List<Card>();
            CardFactory cardFactory = new CardFactory();
            if (idx == 0) {
                deck.Add(cardFactory.getCard("NOVICE"));
                deck.Add(cardFactory.getCard("NOVICE"));
                deck.Add(cardFactory.getCard("LOOT"));
                deck.Add(cardFactory.getCard("LOOT"));
                deck.Add(cardFactory.getCard("MAGMA"));
                deck.Add(cardFactory.getCard("MAGMA"));
                deck.Add(cardFactory.getCard("YETI"));
                deck.Add(cardFactory.getCard("YETI"));
                deck.Add(cardFactory.getCard("BLDRFIST"));
                deck.Add(cardFactory.getCard("BLDRFIST"));
            }
            else if (idx == 1) {
                deck.Add(cardFactory.getCard("BLDFEN"));
                deck.Add(cardFactory.getCard("BLDFEN"));
                deck.Add(cardFactory.getCard("RVRCROC"));
                deck.Add(cardFactory.getCard("RVRCROC"));
                deck.Add(cardFactory.getCard("OASISSJW"));
                deck.Add(cardFactory.getCard("OASISSJW"));
                deck.Add(cardFactory.getCard("WARGOLEM"));
                deck.Add(cardFactory.getCard("WARGOLEM"));
            }
            else if (idx == 2) {
                deck.Add(cardFactory.getCard("Inner Rage", 0));
                deck.Add(cardFactory.getCard("Inner Rage", 0));
                deck.Add(cardFactory.getCard("Execute", 1));
                deck.Add(cardFactory.getCard("Whirlwind", 1));
                deck.Add(cardFactory.getCard("Whirlwind", 1));
                deck.Add(cardFactory.getCard("Clockwork Gnome", 1));
                deck.Add(cardFactory.getCard("Clockwork Gnome", 1));
                deck.Add(cardFactory.getCard("Fiery War Axe", 2));
                deck.Add(cardFactory.getCard("Battle Rage", 2));
                deck.Add(cardFactory.getCard("Cleave", 2));
                deck.Add(cardFactory.getCard("Cleave", 2));
                deck.Add(cardFactory.getCard("Acidic Swamp Ooze", 2));
                deck.Add(cardFactory.getCard("Acidic Swamp Ooze", 2));
                deck.Add(cardFactory.getCard("Amani Berserker", 2));
                deck.Add(cardFactory.getCard("Amani Berserker", 2));
                deck.Add(cardFactory.getCard("Charge", 3));
                deck.Add(cardFactory.getCard("Acolyte of Pain", 3));
                deck.Add(cardFactory.getCard("Acolyte of Pain", 3));
                deck.Add(cardFactory.getCard("Raging Worgen", 3));
                deck.Add(cardFactory.getCard("Raging Worgen", 3));
                deck.Add(cardFactory.getCard("Warsong Commander", 3));
                deck.Add(cardFactory.getCard("Warsong Commander", 3));
                deck.Add(cardFactory.getCard("Arathi Weaponmaster", 4));
                deck.Add(cardFactory.getCard("Arathi Weaponmaster", 4));
                deck.Add(cardFactory.getCard("Jeeves", 4));
                deck.Add(cardFactory.getCard("Kor'kron Elite", 4));
                deck.Add(cardFactory.getCard("Kor'kron Elite", 4));
                deck.Add(cardFactory.getCard("Arcanite Reaper", 5));
                deck.Add(cardFactory.getCard("Gurubashi Berserker", 5));
                deck.Add(cardFactory.getCard("Gurubashi Berserker", 5));
            }
            else if (idx == 3) {
                deck.Add(cardFactory.getCard("Blessing of Might", 1));
                deck.Add(cardFactory.getCard("Blessing of Might", 1));
                deck.Add(cardFactory.getCard("Blessing of Wisdom", 1));
                deck.Add(cardFactory.getCard("Hand of Protection", 1));
                deck.Add(cardFactory.getCard("Hand of Protection", 1));
                deck.Add(cardFactory.getCard("Argent Squire", 1));
                deck.Add(cardFactory.getCard("Argent Squire", 1));
                deck.Add(cardFactory.getCard("Clockwork Gnome", 1));
                deck.Add(cardFactory.getCard("Clockwork Gnome", 1));
                deck.Add(cardFactory.getCard("Acidic Swamp Ooze", 2));
                deck.Add(cardFactory.getCard("Acidic Swamp Ooze", 2));
                deck.Add(cardFactory.getCard("Argent Protector", 2));
                deck.Add(cardFactory.getCard("Dire Wolf Alpha", 2));
                deck.Add(cardFactory.getCard("Haunted Creeper", 2));
                deck.Add(cardFactory.getCard("Haunted Creeper", 2));
                deck.Add(cardFactory.getCard("Knife Juggler", 2));
                deck.Add(cardFactory.getCard("Shielded Minibot", 2));
                deck.Add(cardFactory.getCard("Aldor Peacekeeper", 3));
                deck.Add(cardFactory.getCard("Blood Knight", 3));
                deck.Add(cardFactory.getCard("Raid Leader", 3));
                deck.Add(cardFactory.getCard("Raid Leader", 3));
                deck.Add(cardFactory.getCard("Shattered Sun Cleric", 3));
                deck.Add(cardFactory.getCard("Shattered Sun Cleric", 3));
                deck.Add(cardFactory.getCard("Truesilver Champion", 4));
                deck.Add(cardFactory.getCard("Truesilver Champion", 4));
                deck.Add(cardFactory.getCard("Blessing of Kings", 4));
                deck.Add(cardFactory.getCard("Dark Iron Dwarf", 4));
                deck.Add(cardFactory.getCard("Enhance-o Mechano", 4));
                deck.Add(cardFactory.getCard("Jeeves", 4));
                deck.Add(cardFactory.getCard("Stormwind Champion", 7));
            }
            else {
                for (int i = 0; i < 30; i++) {
                    deck.Add(cardFactory.getCard("NOVICE"));
                }
            }
            return deck;
        }
    }
}
