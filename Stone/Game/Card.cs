﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Stone.Game.Rules;

namespace Stone.Game {
    class Card {
        //General Properties
        private String name;
        private int cost;
        private CardType type;

        //Minion Properties
        private int health;
        private int attack;
        private bool readiness; //has minion attacked already
        private bool sickness; //minion summoned this turn
        private List<Rule> rules;

        private void CardInitializer(String name, int cost, CardType type, 
                                     int attack, int health, List<Rule> rules) {
            this.name = name;
            this.cost = cost;
            this.type = type;

            this.attack = attack;
            this.health = health;
            this.readiness = true;
            this.sickness = true;
            this.rules = rules;
        }
        
        public Card(String name, int cost, CardType type) {
            this.CardInitializer(name, cost, type, 0, 0, null);
        }

        //only for minions
        public Card(String name, int cost, int attack, int health, List<Rule> rules) {
            CardInitializer(name, cost, CardType.Minion, attack, health, rules);
        }

        public String GetName() {
            return this.name;
        }

        public int GetCost() {
            return this.cost;
        }

        public CardType GetCardType() {
            return type;
        }

        public int GetAttack() {
            return this.attack;
        }

        public int GetHealth() {
            return this.health;
        }

        public bool GetReadiness() {
            return readiness;
        }

        public bool HasSickness() {
            return this.sickness;
        }

        public void SetReadiness(bool ready) {
            this.readiness = ready;
        }

        public void SetSickness(bool sickness) {
            this.sickness = sickness;
        }

        public List<Rule> GetRules() {
            return this.rules;
        }

        public void DamageSelf(int damage) {
            this.health -= damage;
        }

        public override string ToString() {
            return this.GetName();
        }

    }
}
