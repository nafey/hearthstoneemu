﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Stone.Game.Rules;

namespace Stone.Game {
    class CardFactory {
        public CardFactory() {
        }
        public Card getCard(String name, int c) {
            List<Rule> rules = new List<Rule>();
            Card card = new Card(name, c, 2, 2, rules);
            return card;
        }

        public Card getCard(String name, int c, int a, int h) {
            List<Rule> rules = new List<Rule>();
            Card card = new Card(name, c, a, h, rules);
            return card;
        }

        public Card getCard(String name) {
            List<Rule> rules = new List<Rule>();
            Card card = null;
            if (name == "NOVICE") {
                rules.Add(new Rule(new Condition(), new DrawCardEffect(true, 1), RuleTrigger.BattleCry)); 
                card = new Card("Novice Engineer", 2, 1, 1, rules);
            }
            else if (name == "LOOT") {
                rules.Add(new Rule(new Condition(), new DrawCardEffect(true, 1), RuleTrigger.DeathRattle));
                card = new Card("Loot Hoarder", 2, 2, 1, rules);
            }
            else if (name == "MAGMA") {
                card = new Card("Magma Rager", 3, 5, 1, rules);
            }
            else if (name == "YETI") {
                card = new Card("Chillwind Yeti", 4, 4, 5, rules);
            }
            else if (name == "BLDRFIST") {
                card = new Card("Boulderfist Ogre", 6, 6, 7, rules);
            }
            else if (name == "BLDFEN") {
                card = new Card("Bloodfen Raptor", 2, 3, 2, rules);
            }
            else if (name == "RVRCROC") {
                card = new Card("River Crocolisk", 2, 2, 3, rules);
            }
            else if (name == "OASISSJW") {
                card = new Card("Oasis Snapjaw", 4, 2, 7, rules);
            }
            else if (name == "WARGOLEM") {
                card = new Card("War Golem", 7, 7, 7, rules);
            }
            else if (name == "") {

            }
            else {
                card = new Card(name, 2, CardType.Minion);
            }
           
            return card;
        }
    }
}
