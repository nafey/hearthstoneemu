﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Stone.Game;
using Stone.Game.Rules;

namespace Stone.Game {
    class Player {
        private String name;

        private List<Card> deck;
        private List<Card> hand;
        private List<Card> table;
        private List<Card> limbo;

        private List<Rules.Rule> rules;

        private Card playedCard;

        private int health;
        private int currentMana;
        private int totalMana;
        private Random rnd;
        private Player target;
        private bool myTurn;

        public delegate void ExecuteRuleDelegateType(RuleTrigger trigger);
        private ExecuteRuleDelegateType executeRuleDelegate;

        private void PlayerClassInitializer(int x) {
            this.rnd = new Random();

            //Mana init
            this.currentMana = 0;
            this.totalMana = 0;

            //Health init
            this.health = 30;

            //initialize rule book
            this.rules = new List<Rules.Rule>();

            //initialize deck
            this.deck = new List<Card>();
            DeckFactory deckFactory = new DeckFactory();
            if (x == 0) {
                name = "Gavlan";
                deck = deckFactory.getDeck(0);
            }
            else {
                name = "Sylvanas";
                deck = deckFactory.getDeck(1);
            }

            this.Shuffle();

            //initialize hand
            this.hand = new List<Card>();

            //initialize table
            this.table = new List<Card>();

            //initialize limbo
            this.limbo = new List<Card>();

            //set my turn
            myTurn = false;
        }

        public Player(int x, ExecuteRuleDelegateType executeRuleDelegate) {
            this.PlayerClassInitializer(x);
            this.executeRuleDelegate += executeRuleDelegate;
        }

        public Card GetPlayedCard() {
            return this.playedCard;
        }

        public List<Card> GetLimbo() {
            return this.limbo;
        }

        public List<Card> GetTable() {
            return this.table;
        }

        public PlayerInfo GetPlayerInfo() {
            return new PlayerInfo(this.GetHandFacade(), this.GetTableFacade(), this.deck.Count, this.health,
                this.currentMana, this.totalMana, this.name, this.myTurn);
        }

        private List<CardFacade> GetTableFacade() {
            List<CardFacade> ret = new List<CardFacade>();
            foreach (Card c in this.table) {
                ret.Add(new CardFacade(c));
            }
            return ret;
        }

        private List<CardFacade> GetHandFacade() {
            List<CardFacade> ret = new List<CardFacade>();
            foreach (Card c in this.hand) {
                ret.Add(new CardFacade(c));
            }
            return ret;
        }
        
        private void Swap(int x1, int x2) {
            Card c1 = this.deck[x1];
            this.deck[x1] = this.deck[x2];
            this.deck[x2] = c1;
        }

        //Execute turn start stuff
        public void BeginTurn() {
            //increase available mana
            this.IncreaseMana(1, true);

            //Draw a card
            this.Draw();

            //refresh all the mana
            this.RefreshMana();

            //Enable attack on all current creatures
            foreach (Card c in this.table) {
                c.SetReadiness(true);
                c.SetSickness(false);
            }

            //its my turn
            this.myTurn = true;
        }

        //shuffles the deck
        public void Shuffle() {
            int i_rand = 0;
            for (int i = 1; i < this.deck.Count; i++) {
                i_rand = rnd.Next(0, i + 1);
                this.Swap(i, i_rand);
            }
        }

        //End your turn
        public void EndTurn() {
            //no longer my turn
            this.myTurn = false;
        }
        
        //draw a card from your deck and add it to your hand
        public void Draw() {
            //Draw only if card available in deck
            if (this.deck.Count > 0) {
                Card c = this.deck[0];
                this.deck.RemoveAt(0);
                this.hand.Add(c);
            }
        }

        public void GoFirst() {
            //Draw 3 cards
            this.Draw();
            this.Draw();
            this.Draw();
        }

        public void GoSecond() {
            //Draw 4 cards
            this.Draw();
            this.Draw();
            this.Draw();
            this.Draw();

            //Draw a coin
            //implement later
        }

        //increase total mana
        public void IncreaseMana(int num, bool active) {
            if ((num > 0) && (this.totalMana <= 10)) {
                this.totalMana += num;
                if (active) this.currentMana += num;
            }
        }

        //make all the mana available
        public void RefreshMana() {
            this.currentMana = this.totalMana;
        }

        //increase available mana
        public void EnhanceMana(int num) {
            if (num > 0) {
                this.currentMana += num;
            }
        }

        //Consume available mana
        public void UseMana(int cost) {
            if (cost <= this.currentMana) {
                this.currentMana -= cost;
            }
            else {
                Console.WriteLine("Using more mana than available");
            }
        }

        //Check if card can be summoned
        private bool CheckSummon(Card c) {
            int cost = c.GetCost();

            //Summon only if there is enough mana
            if (c.GetCost() > currentMana) {
                return false;
            }

            return true;
        }

        //Selected card in my hand is summoned
        public void PlayFromHand(int i) {
            Card c = null;
            c = hand[i];

            //if cant be summoned stop the process
            if (!this.CheckSummon(c)) return;

            //Consume mana
            this.UseMana(c.GetCost());
            
            //clear the card from hand
            this.hand.RemoveAt(i);

            //set as played card
            this.playedCard = c;

            //execute battlecry
            this.executeRuleDelegate(RuleTrigger.BattleCry);
            
            //remove played card
            this.playedCard = null;

            //add to the table
            this.table.Add(c);

            //the card cant attack
            c.SetReadiness(false);
        }

        //Set the target player
        public void SetTarget(Player p) {
            if (p != this) {
                this.target = p;
            }
        }

        //Player takes damage
        public void DamageSelf(int damage) {
            this.health -= damage;
        }

        //Minion takes damage
        public void DamageMinion(int defenderIndex, int damage) {
            Card defender = this.table[defenderIndex];
            defender.DamageSelf(damage);

            if (defender.GetHealth() <= 0) {
                this.limbo.Add(defender);
                this.table.Remove(defender);
            }
        }

        //Selected card attacks the target player
        public void AttackFace(int attackerIndex) {
            if (attackerIndex > this.table.Count) {
                Console.WriteLine("Incorrect i for AttackFace");
                return;
            }

            Card attacker = this.table[attackerIndex];

            //has already attacked
            if (!attacker.GetReadiness()) return;

            //if summoned this turn cant attack
            if (attacker.HasSickness()) return;

            this.target.DamageSelf(attacker.GetAttack());

            attacker.SetReadiness(false);
        }

        //Selected card attacks the target minion
        public void AttackMinion(int attackerIndex, int defenderIndex) {
            if (attackerIndex > this.table.Count) {
                Console.WriteLine("Incorrect i for AttackFace");
                return;
            }

            Card attacker = this.table[attackerIndex];
            Card defender = this.target.GetTable()[defenderIndex];

            int attackerAttack = attacker.GetAttack();
            int defenderAttack = defender.GetAttack();

            //has already attacked
            if (!attacker.GetReadiness()) return;

            //if summoned this turn cant attack
            if (attacker.HasSickness()) return;

            //Exchange damage with attacker
            this.target.DamageMinion(defenderIndex, attackerAttack);
            this.DamageMinion(attackerIndex, defenderAttack);

            //trigger deathrattle
            this.executeRuleDelegate(RuleTrigger.DeathRattle);

            //empty limbo
            this.limbo.Clear();
            
            attacker.SetReadiness(false);
        }

    }
}
