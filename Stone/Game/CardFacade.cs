﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stone.Game {
    class CardFacade {
        String name;
        int cost;
        int attack;
        int health;
        string ready;
                
        public CardFacade(Card card) {
            this.name = card.GetName();
            this.cost = card.GetCost();
            
            this.attack = card.GetAttack();
            this.health = card.GetHealth();

            this.ready = "";

            if (card.GetReadiness() && !card.HasSickness()) ready = " R";

        }

        public String GetName() {
            return name;
        }

        public int GetCost() {
            return cost;
        }

        public int GetAttack() {
            return attack;
        }

        public int GetHealth() {
            return health;
        }

        public String GetReady() {
            return ready;
        }

        public override string ToString() {
            return name;
        }

    }
}
