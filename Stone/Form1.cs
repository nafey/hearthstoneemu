﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Stone.Game;

namespace Stone {
    public partial class Form1 : Form {
        Game.Game game;
        int active = 0;

        private void RefreshComponents() {
            List<PlayerInfo> playerInfo = game.GetPlayerInfo();

            //Set player names
            this.lblName1.Text = playerInfo[0].GetName();
            this.lblName2.Text = playerInfo[1].GetName();

            //refresh the deck count
            this.nupDeck1.Value = playerInfo[0].GetDeckSize();
            this.nupDeck2.Value = playerInfo[1].GetDeckSize();

            //refresh the hand lists
            this.lstHand1.Items.Clear();
            foreach (CardFacade c in playerInfo[0].GetHand()) {
                this.lstHand1.Items.Add(c + " " + c.GetCost());
            }
            this.lstHand2.Items.Clear();
            foreach (CardFacade c in playerInfo[1].GetHand()) {
                this.lstHand2.Items.Add(c + " " + c.GetCost());
            }

            //refresh the table lists
            this.lstTable1.Items.Clear();
            foreach (CardFacade c in playerInfo[0].GetTable()) {
                this.lstTable1.Items.Add(c + " " + c.GetAttack() + " " + c.GetHealth() + " " + c.GetReady());
            }
            this.lstTable2.Items.Clear();
            foreach (CardFacade c in playerInfo[1].GetTable()) {
                this.lstTable2.Items.Add(c + " " + c.GetAttack() + " " + c.GetHealth() + " " + c.GetReady());
            }

            //set the active player. 
            if (playerInfo[0].GetIsMyTurn()) {
                lblName1.Text += " -- ACTIVE";
                lblName2.Text = playerInfo[1].GetName();

                active = 0;
            }
            else {
                lblName2.Text += " -- ACTIVE";
                lblName1.Text = playerInfo[0].GetName();

                active = 1;
            }

            //Show Health
            this.nupHealth1.Value = playerInfo[0].GetHealth();
            this.nupHealth2.Value = playerInfo[1].GetHealth();

            //rerfresh the mana count
            this.txtMana1.Text = playerInfo[0].GetCurrentMana() + "/" + playerInfo[0].GetTotalMana();
            this.txtMana2.Text = playerInfo[1].GetCurrentMana() + "/" + playerInfo[1].GetTotalMana();
        }

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            this.game = new Game.Game();
            this.RefreshComponents();
        }

        private void btnEndTurn1_Click(object sender, EventArgs e) {
            game.TurnEndAction();
            this.RefreshComponents();
        }

        private void btnSummon_Click(object sender, EventArgs e) {
            //Get selected card for the active player
            int sel = 0;
            if (active == 0) {
                sel = this.lstHand1.SelectedIndex;
            }
            else {
                sel = this.lstHand2.SelectedIndex;
            }

            if (sel >= 0) {
                game.PlayCardAction(sel);
            }

            this.RefreshComponents();
        }

        private void btnAttackFace_Click(object sender, EventArgs e) {
            int sel = 0; //index of the attacker
            int plr = 0; //the player to be attacked

            if (active == 0) {
                sel = this.lstTable1.SelectedIndex;
                plr = 1;
            }
            else {
                sel = this.lstTable2.SelectedIndex;
                plr = 0;
            }

            if (sel >= 0) {
                this.game.AttackFaceAction(sel, plr);
            }

            this.RefreshComponents();
        }

        private void btnAttack_Click(object sender, EventArgs e) {
            int att = 0; //index of the attacker
            int def = 0; //index of the defender
            int trg = 0; //the player to be attacked
            

            if (active == 0) {
                att = this.lstTable1.SelectedIndex;
                def = this.lstTable2.SelectedIndex;
                trg = 1;
            }
            else {
                att = this.lstTable2.SelectedIndex;
                def = this.lstTable1.SelectedIndex;
                trg = 0;
            }

            if (att >= 0) {
                this.game.AttackMinionAction(att, def, trg);
            }

            this.RefreshComponents();
        }
    }
}
