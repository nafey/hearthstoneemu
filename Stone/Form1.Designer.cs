﻿namespace Stone
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName1 = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.nupDeck1 = new System.Windows.Forms.NumericUpDown();
            this.nupDeck2 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lstHand1 = new System.Windows.Forms.ListBox();
            this.lstHand2 = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMana1 = new System.Windows.Forms.TextBox();
            this.txtMana2 = new System.Windows.Forms.TextBox();
            this.btnEndTurn1 = new System.Windows.Forms.Button();
            this.lstTable1 = new System.Windows.Forms.ListBox();
            this.lstTable2 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSummon = new System.Windows.Forms.Button();
            this.btnAttackFace = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.nupHealth1 = new System.Windows.Forms.NumericUpDown();
            this.nupHealth2 = new System.Windows.Forms.NumericUpDown();
            this.btnAttack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nupDeck1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupDeck2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupHealth1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupHealth2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName1
            // 
            this.lblName1.AutoSize = true;
            this.lblName1.Location = new System.Drawing.Point(43, 9);
            this.lblName1.Name = "lblName1";
            this.lblName1.Size = new System.Drawing.Size(42, 13);
            this.lblName1.TabIndex = 0;
            this.lblName1.Text = "Player1";
            // 
            // lblName2
            // 
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(242, 9);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(42, 13);
            this.lblName2.TabIndex = 1;
            this.lblName2.Text = "Player2";
            // 
            // nupDeck1
            // 
            this.nupDeck1.Location = new System.Drawing.Point(46, 42);
            this.nupDeck1.Name = "nupDeck1";
            this.nupDeck1.Size = new System.Drawing.Size(39, 20);
            this.nupDeck1.TabIndex = 2;
            // 
            // nupDeck2
            // 
            this.nupDeck2.Location = new System.Drawing.Point(245, 42);
            this.nupDeck2.Name = "nupDeck2";
            this.nupDeck2.Size = new System.Drawing.Size(39, 20);
            this.nupDeck2.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Deck";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(206, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Deck";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Hand";
            // 
            // lstHand1
            // 
            this.lstHand1.FormattingEnabled = true;
            this.lstHand1.Location = new System.Drawing.Point(46, 76);
            this.lstHand1.Name = "lstHand1";
            this.lstHand1.Size = new System.Drawing.Size(120, 134);
            this.lstHand1.TabIndex = 7;
            // 
            // lstHand2
            // 
            this.lstHand2.FormattingEnabled = true;
            this.lstHand2.Location = new System.Drawing.Point(245, 76);
            this.lstHand2.Name = "lstHand2";
            this.lstHand2.Size = new System.Drawing.Size(121, 134);
            this.lstHand2.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(206, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Hand";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 348);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Mana";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 348);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Mana";
            // 
            // txtMana1
            // 
            this.txtMana1.Location = new System.Drawing.Point(46, 345);
            this.txtMana1.Name = "txtMana1";
            this.txtMana1.Size = new System.Drawing.Size(120, 20);
            this.txtMana1.TabIndex = 15;
            // 
            // txtMana2
            // 
            this.txtMana2.Location = new System.Drawing.Point(245, 345);
            this.txtMana2.Name = "txtMana2";
            this.txtMana2.Size = new System.Drawing.Size(121, 20);
            this.txtMana2.TabIndex = 16;
            // 
            // btnEndTurn1
            // 
            this.btnEndTurn1.Location = new System.Drawing.Point(291, 392);
            this.btnEndTurn1.Name = "btnEndTurn1";
            this.btnEndTurn1.Size = new System.Drawing.Size(75, 23);
            this.btnEndTurn1.TabIndex = 17;
            this.btnEndTurn1.Text = "End Turn";
            this.btnEndTurn1.UseVisualStyleBackColor = true;
            this.btnEndTurn1.Click += new System.EventHandler(this.btnEndTurn1_Click);
            // 
            // lstTable1
            // 
            this.lstTable1.FormattingEnabled = true;
            this.lstTable1.Location = new System.Drawing.Point(46, 216);
            this.lstTable1.Name = "lstTable1";
            this.lstTable1.Size = new System.Drawing.Size(120, 95);
            this.lstTable1.TabIndex = 18;
            // 
            // lstTable2
            // 
            this.lstTable2.FormattingEnabled = true;
            this.lstTable2.Location = new System.Drawing.Point(245, 216);
            this.lstTable2.Name = "lstTable2";
            this.lstTable2.Size = new System.Drawing.Size(121, 95);
            this.lstTable2.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(206, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Table";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Table";
            // 
            // btnSummon
            // 
            this.btnSummon.Location = new System.Drawing.Point(210, 392);
            this.btnSummon.Name = "btnSummon";
            this.btnSummon.Size = new System.Drawing.Size(75, 23);
            this.btnSummon.TabIndex = 22;
            this.btnSummon.Text = "Summon";
            this.btnSummon.UseVisualStyleBackColor = true;
            this.btnSummon.Click += new System.EventHandler(this.btnSummon_Click);
            // 
            // btnAttackFace
            // 
            this.btnAttackFace.Location = new System.Drawing.Point(129, 392);
            this.btnAttackFace.Name = "btnAttackFace";
            this.btnAttackFace.Size = new System.Drawing.Size(75, 23);
            this.btnAttackFace.TabIndex = 23;
            this.btnAttackFace.Text = "AttackFace";
            this.btnAttackFace.UseVisualStyleBackColor = true;
            this.btnAttackFace.Click += new System.EventHandler(this.btnAttackFace_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(91, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Health";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(290, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Health";
            // 
            // nupHealth1
            // 
            this.nupHealth1.Location = new System.Drawing.Point(127, 42);
            this.nupHealth1.Name = "nupHealth1";
            this.nupHealth1.Size = new System.Drawing.Size(39, 20);
            this.nupHealth1.TabIndex = 26;
            // 
            // nupHealth2
            // 
            this.nupHealth2.Location = new System.Drawing.Point(327, 42);
            this.nupHealth2.Name = "nupHealth2";
            this.nupHealth2.Size = new System.Drawing.Size(39, 20);
            this.nupHealth2.TabIndex = 27;
            // 
            // btnAttack
            // 
            this.btnAttack.Location = new System.Drawing.Point(48, 392);
            this.btnAttack.Name = "btnAttack";
            this.btnAttack.Size = new System.Drawing.Size(75, 23);
            this.btnAttack.TabIndex = 28;
            this.btnAttack.Text = "Attack";
            this.btnAttack.UseVisualStyleBackColor = true;
            this.btnAttack.Click += new System.EventHandler(this.btnAttack_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 476);
            this.Controls.Add(this.btnAttack);
            this.Controls.Add(this.nupHealth2);
            this.Controls.Add(this.nupHealth1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnAttackFace);
            this.Controls.Add(this.btnSummon);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstTable2);
            this.Controls.Add(this.lstTable1);
            this.Controls.Add(this.btnEndTurn1);
            this.Controls.Add(this.txtMana2);
            this.Controls.Add(this.txtMana1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lstHand2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lstHand1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nupDeck2);
            this.Controls.Add(this.nupDeck1);
            this.Controls.Add(this.lblName2);
            this.Controls.Add(this.lblName1);
            this.Name = "Form1";
            this.Text = "Brimstone";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupDeck1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupDeck2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupHealth1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupHealth2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName1;
        private System.Windows.Forms.Label lblName2;
        private System.Windows.Forms.NumericUpDown nupDeck1;
        private System.Windows.Forms.NumericUpDown nupDeck2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox lstHand1;
        private System.Windows.Forms.ListBox lstHand2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMana1;
        private System.Windows.Forms.TextBox txtMana2;
        private System.Windows.Forms.Button btnEndTurn1;
        private System.Windows.Forms.ListBox lstTable1;
        private System.Windows.Forms.ListBox lstTable2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSummon;
        private System.Windows.Forms.Button btnAttackFace;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nupHealth1;
        private System.Windows.Forms.NumericUpDown nupHealth2;
        private System.Windows.Forms.Button btnAttack;

    }
}

